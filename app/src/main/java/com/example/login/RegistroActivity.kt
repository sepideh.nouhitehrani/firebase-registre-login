package com.example.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField


import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.login.ui.theme.LoginTheme

import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.login.ui.theme.Purple40
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser




class RegistroActivity : ComponentActivity() {
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()
        setContent {
            RegistroScreen()
            /*LoginTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Registro()
                }
            }*/
        }
    }
    @Composable
    fun RegistroScreen() {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Purple40
        ) {
            RegistroContent()
        }
    }
    @Composable
    fun RegistroContent() {
        var email by remember { mutableStateOf("") }
        var password by remember { mutableStateOf("") }

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextField(
                value = email,
                onValueChange = { email = it },
                label = { Text(text = "Email", color = Color.White) },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email,
                    imeAction = ImeAction.Next
                ),
                modifier = Modifier.padding(16.dp)
            )

            TextField(
                value = password,
                onValueChange = { password = it },
                label = { Text(text = "Password", color = Color.White) },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password,
                    imeAction = ImeAction.Done
                ),
                modifier = Modifier.padding(16.dp)
            )

            Button(
                onClick = {
                    registro(email, password)
                },
                modifier = Modifier.padding(16.dp)
            ) {
                Text(text = "Register")
            }
        }
    }

    fun registro(email: String, password: String) {
//verificar que no están vacías
        if (email.isNotEmpty() && password.isNotEmpty()) {
            firebaseAuth.createUserWithEmailAndPassword(email, password) //signInWithEmailAndPassword
                .addOnCompleteListener(this) {
                    val user: FirebaseUser = firebaseAuth.currentUser!!
                    verifyEmail(user)
                    startActivity(Intent(this, Mostrar::class.java))
                }.addOnFailureListener {
                    Toast.makeText(
                        this,
                        "Authentication error",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        } else {
            Toast.makeText(
                this,
                "Insert correct data",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun verifyEmail(user: FirebaseUser) {
        user.sendEmailVerification().addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(
                    this,
                    "Email verified",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    this,
                    "Error verifying email",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview() {
        LoginTheme {
            RegistroContent()
        }
    }
}