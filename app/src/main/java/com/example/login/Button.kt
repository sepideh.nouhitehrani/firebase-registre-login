
package com.example.login

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.login.ui.theme.LoginTheme
import com.google.firebase.auth.FirebaseAuth
import kotlin.random.Random

class Button : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LoginTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    app()
                }
            }
        }
    }
}

@Composable
fun app() {
    var state by remember {
        mutableStateOf(GardenState.EMPTY)
    }

    var earnings by remember {
        mutableStateOf(0)
    }

    Column {
        when (state) {
            GardenState.EMPTY -> {
                Image(
                    painterResource(id = R.drawable.one) ,
                    contentDescription = "Empty Garden",
                    modifier = Modifier.weight(1f)
                )
            }
            GardenState.PLANTS -> Image(
                painterResource(id = R.drawable.two),
                contentDescription = "Plants in Garden",
                modifier = Modifier.weight(1f)
            )
            GardenState.FRUIT -> Image(
                painterResource(id = com.example.login.R.drawable.three),
                contentDescription = "Fruits in Garden",
                modifier = Modifier.weight(1f)
            )
        }

        Button(
            onClick = {
                when (state) {
                    GardenState.EMPTY -> {
                        state = GardenState.PLANTS
                    }
                    GardenState.PLANTS -> {
                        state = GardenState.FRUIT
                    }
                    GardenState.FRUIT -> {
                        val earningsToday = Random.nextInt(1, 6)
                        earnings += earningsToday
                        state = GardenState.EMPTY
                    }
                }
            },
            modifier = Modifier.padding(16.dp)
        ) {
            Text(text = "Press Me")
        }

        Text(
            text = "Today's Earnings: €$earnings",
            modifier = Modifier.padding(16.dp)
        )
    }
}

enum class GardenState {
    EMPTY,
    PLANTS,
    FRUIT
}
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    LoginTheme {
        app()
    }


}